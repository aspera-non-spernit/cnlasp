extern crate serde_yaml;
/***
 * Controlled Natural Language Processing as Answer Set Programming: an Experiment
 * A Rust Implementation of the experimental way to parsing contolled natural language
 * for ASP
 */
use cnlasp::{InputText, Lexicon, Token, Tokenize};
use std::{ convert::{TryFrom, TryInto}, fs::File, io::{prelude::*, BufReader}};

const EXAMPLE_TEXT: &str = "Every student who works and who is not provably absent is successful.If a student does not provably work then the student does not work.John is a student who works.Sue is a student and works.Mary Ann who is a student is absent.Exclude that a student who cheats is successful.";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let tokens = InputText::tokenize(EXAMPLE_TEXT);
    //tokens.iter().for_each(|t| println!("{}", t) );
    let token = Token::try_from("token(works,0,3,4).".to_string());
    dbg!(  &token );
    let asp_token: Result<String, _> = token.try_into();
    dbg!( asp_token);
    Ok(())
}