extern crate serde;
use serde::{Deserialize, Serialize, Serializer};
use std::{fs::File, convert::TryFrom, io::{Read, BufReader}};

#[derive(Debug, Deserialize, Serialize)] pub enum Category { Adjective, Conjunction, Determiner, Iv, Noun, RelativePronoun }
#[derive(Debug, Deserialize, Serialize)] pub enum SyntacticConstraint { Sg }        //Nil not used. Use None
#[derive(Debug, Deserialize, Serialize)] pub enum SemanticConstraint { ForAll }     //Nil not used. Use None

pub struct InputText;

#[derive(Debug, Deserialize, Serialize)]
pub struct LexicalEntry {
    pub string: String,
    pub category: Category,
    pub base: Option<String>,
    pub syntactic_contraint: Option<SyntacticConstraint>,
    pub semantic_constraint: Option<SemanticConstraint>
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Lexicon { 
    entries: Vec<LexicalEntry>
}

#[derive(Debug, Deserialize)] // impl Serialize
pub struct Token {
    pub string: String,
    pub sentence: u8,
    pub begin: u8,
    pub end: u8
}

impl TryFrom<String> for Token {
    type Error = &'static str;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        // example: valid asp token token(works,0,3,4).
        let mut ss = s.split("token(")
            .collect::<String>()
            .split(')')
            .collect::<String>();
        ss = ss.trim_end_matches('.').to_string();
        let sj: Vec<&str> = ss.split(',').collect();     
        Ok(
            Token {
                string: sj[0].to_string(),
                sentence: sj[1].to_string().parse::<u8>().unwrap(),
                begin: sj[2].to_string().parse::<u8>().unwrap(),
                end: sj[3].to_string().parse::<u8>().unwrap()
            }
        )
    }
}


pub trait Tokenize { fn tokenize(text: &str) -> Vec<Token>; }

// impl Serialize for Token {
//     fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
//         where S: Serializer, {
//             let mut state = serializer.serialize_struct("Token", 4)?;
//             state.serialize_field("r", &self.string)?;
//             state.serialize_field("g", &self.sentence)?;
//             state.serialize_field("b", &self.start)?;
//             state.serialize_field("b", &self.end)?;

//             state.end()
//         }
// }

/***
 * Displays a Token as ASP Fact
 */
impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {

        write!(f, "fmt token")
    }
}

impl TryFrom<File> for Token {
    type Error = &'static str;

    fn try_from(value: File) -> Result<Token, Self::Error> {
        match File::open("data/tokens.") {
            Ok(f) => { 
                let mut buf_reader = BufReader::new(f);
                let mut contents = String::new();
                match buf_reader.read_to_string(&mut contents) {
                    Ok(size) => {
                        dbg!(&contents);
                        Ok(
                            Token {
                                string: "".to_string(),
                                sentence: 1,
                                begin: 0,
                                end: 3
                            }
                        )
                    },
                    Err(e) => Err("Error reading data from tokens.lp"),
                }
            },
            Err(e) => Err("Error opening tokens.lp"),
        } 

     
    }
}
impl Tokenize for InputText {
      /**
    In order to process this text in ASP, we split it into a sequence of sentences
    and each sentence into a sequence of tokens. Each token is represented in ASP
    as a fact ( token/4 ) with four arguments: the first argument holds the string, the
    second argument holds the sentence number, and the third and fourth argument
    represent the start and the end position of the string, for example (p.3)
    */
    fn tokenize(text: &str) -> Vec<Token> {
        let sentences = text.split('.')
            .filter(|s| s.len() != 0)
            .collect::<Vec<&str>>();

        let mut tokens = vec![];
        let tokenized = sentences
        .iter()
        .enumerate()
        .map(|(i, s)| {
            s.to_string().split_whitespace()
                .map(|ss| {
                    ss.to_string()
                }).collect::<Vec<String>>()
        })
        .collect::<Vec<Vec<String>>>();
        for (i, s) in tokenized.iter().enumerate() {
            for (j, t) in s.iter().enumerate() {
                tokens.push(
                    Token {
                        string: t.to_string(),
                        sentence: i as u8,
                        begin: j as u8,
                        end: (j + 1) as u8
                    }
                );
            }    
        }
        tokens
    }
}