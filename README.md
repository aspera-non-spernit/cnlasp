# CNLASP

Based on the research paper **"Controlled Natural Language Processing as
Answer Set Programming: an Experiment"** by Rolf Schwitter <Rolf.Schwitter@mq.edu.au> and as support to my studies
at the University of Potsdam, I am trying to implement the work in rust.

The goal is to provide a program that is capable of parsing controlled natural lanuage into an Answer Set Program.



# References

* [Controlled Natural Language Processing as Answer Set Programming: an Experiment](https://arxiv.org/abs/1408.2466v1) (Rolf Switter, 15. July 2014 online)
